#!/bin/bash

cd "$(dirname "$0")"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

# Check autobuild is around or fail
if [ -z "$AUTOBUILD" ] ; then
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# Load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

stage="$(pwd)/stage"
stage_bin_release="$stage/bin/release"
stage_bin_debug="$stage/bin/debug"
stage_lib_release="$stage/lib/release"
stage_lib_debug="$stage/lib/debug"

# Create the staging license folder
mkdir -p "$stage/LICENSES"

# Create the staging include folders
mkdir -p "$stage/include/cef"
mkdir -p "$stage/include/cef/include"

#Create the staging debug and release folders
mkdir -p "$stage_bin_debug"
mkdir -p "$stage_bin_debug/swiftshader"
mkdir -p "$stage_bin_release"
mkdir -p "$stage_bin_release/swiftshader"
mkdir -p "$stage_lib_debug"
mkdir -p "$stage_lib_release"

#Create the staging resource dir
mkdir -p "$stage/resources"

case "$AUTOBUILD_PLATFORM" in
    "darwin")
        export GYP_GENERATORS=xcode
        XCODE_FLAGS="-sdk macosx10.14"
        pushd "cef"
            # Debug
            #sh ./cef_create_projects.sh
            cmake -G "Xcode" . -DPROJECT_ARCH="x86_64" -DCMAKE_BUILD_TYPE=Debug
            xcodebuild -target libcef_dll_wrapper ${XCODE_FLAGS} -configuration Debug
            xcodebuild -target "cefclient_Helper" ${XCODE_FLAGS} -configuration Debug

            cp libcef_dll_wrapper/Debug/libcef_dll_wrapper.a "${stage_lib_debug}"
            cp -R Debug/"Chromium Embedded Framework.framework" "${stage_bin_debug}"
            cp -R tests/cefclient/Debug/"cefclient Helper.app" "${stage_bin_debug}"

            # Release
            #sh ./cef_create_projects.sh
            cmake -G "Xcode" . -DPROJECT_ARCH="x86_64" -DCMAKE_BUILD_TYPE=Release
            xcodebuild -target libcef_dll_wrapper ${XCODE_FLAGS} -configuration Release
            xcodebuild -target "cefclient_Helper" ${XCODE_FLAGS} -configuration Release

            cp libcef_dll_wrapper/Release/libcef_dll_wrapper.a "${stage_lib_release}"
            cp -R Release/"Chromium Embedded Framework.framework" "${stage_bin_release}"
            cp -R tests/cefclient/Release/"cefclient Helper.app" "${stage_bin_release}"

            # Common
            cp -R tests/cefclient/resources/* "${stage}/resources"
            cp -R include/* "$stage/include/cef/include"
        popd
        pushd "dullahan"
            # Debug
            cmake -G "Xcode" . -DPROJECT_ARCH="x86_64" \
                -DCEF_INCLUDE_DIR="$stage/include/cef/include" \
                -DCEF_LIB_DIR="$stage/lib" \
                -DCEF_BIN_DIR="$stage/bin" \
                -DCEF_RESOURCE_DIR="$stage/resources" \
                -DCMAKE_BUILD_TYPE=Debug
            xcodebuild -target dullahan ${XCODE_FLAGS} -configuration Debug
            xcodebuild -target dullahan_host ${XCODE_FLAGS} -configuration Debug

            install_name_tool -delete_rpath "${stage_bin_debug}" "Debug/DullahanHelper.app/Contents/MacOS/DullahanHelper"
            cp "Debug/libdullahan.a" "${stage_lib_debug}"
            cp -R "Debug/DullahanHelper.app" "${stage_bin_debug}"

            # Release
            cmake -G "Xcode" . -DPROJECT_ARCH="x86_64" \
                -DCEF_INCLUDE_DIR="$stage/include/cef/include" \
                -DCEF_LIB_DIR="$stage/lib" \
                -DCEF_BIN_DIR="$stage/bin" \
                -DCEF_RESOURCE_DIR="$stage/resources" \
                -DCMAKE_BUILD_TYPE=Release
            xcodebuild -target dullahan ${XCODE_FLAGS} -configuration Release
            xcodebuild -target dullahan_host ${XCODE_FLAGS} -configuration Release

            install_name_tool -delete_rpath "${stage_bin_release}" "Release/DullahanHelper.app/Contents/MacOS/DullahanHelper"
            cp "Release/libdullahan.a" "${stage_lib_release}"
            cp -R "Release/DullahanHelper.app" "${stage_bin_release}"
        popd
    ;;
    "linux")
        echo "Not implemented"
        fail
     ;;
    "linux64")
        pushd "cef"
            rm -rf build_debug
            mkdir -p build_debug
            pushd "build_debug"
                cmake -G "Unix Makefiles" ../ -DCMAKE_BUILD_TYPE=Debug
                make -j4 libcef_dll_wrapper
                cp libcef_dll_wrapper/libcef_dll_wrapper.a "$stage_lib_debug"
            popd
            rm -rf build_release
            mkdir -p build_release
            pushd "build_release"
                cmake -G "Unix Makefiles" ../ -DCMAKE_BUILD_TYPE=Release
                make -j4 libcef_dll_wrapper
                cp libcef_dll_wrapper/libcef_dll_wrapper.a "$stage_lib_release"
            popd
            cp -a Debug/chrome-sandbox "$stage_bin_debug"
            cp -a Debug/*.bin "$stage_bin_debug"
            cp -a Debug/*.so "$stage_lib_debug"
            cp -a Release/chrome-sandbox "$stage_bin_release"
            cp -a Release/*.bin "$stage_bin_release"
            cp -a Release/*.so "$stage_lib_release"
            cp -a Resources/* "$stage/resources"
            cp -R include/* "$stage/include/cef/include"
        popd
        pushd "dullahan"
            rm -rf build_debug
            mkdir -p build_debug
            pushd "build_debug"
                cmake -G "Unix Makefiles" ../ \
                -DCEF_INCLUDE_DIR="$stage/include/cef/include" \
                -DCEF_LIB_DIR="$stage/lib" \
                -DCEF_BIN_DIR="$stage/bin" \
                -DCEF_RESOURCE_DIR="$stage/resources" \
                -DCMAKE_BUILD_TYPE=Debug
                make -j4
                cp dullahan_host "$stage_bin_debug"
                cp libdullahan.a "$stage_lib_debug"
            popd
            rm -rf build_release
            mkdir -p build_release
            pushd "build_release"
                cmake -G "Unix Makefiles" ../ \
                -DCEF_INCLUDE_DIR="$stage/include/cef/include" \
                -DCEF_LIB_DIR="$stage/lib" \
                -DCEF_BIN_DIR="$stage/bin" \
                -DCEF_RESOURCE_DIR="$stage/resources" \
                -DCMAKE_BUILD_TYPE=Release
                make -j4
                cp dullahan_host "$stage_bin_release"
                cp libdullahan.a "$stage_lib_release"
            popd
        popd
    ;;
    "windows")
        pushd "cef"
            rm -rf build
            mkdir -p build
            pushd "build"
                cmake -G "Visual Studio 15" .. -DCMAKE_SYSTEM_VERSION="10.0.17134.0" -DCEF_RUNTIME_LIBRARY_FLAG="/MD"
                cmake --build . --target "libcef_dll_wrapper" --config "Debug"
                cmake --build . --target "libcef_dll_wrapper" --config "Release"

                cp libcef_dll_wrapper/Debug/libcef_dll_wrapper.* "$stage_lib_debug"
                cp libcef_dll_wrapper/Release/libcef_dll_wrapper.* "$stage_lib_release"
            popd
            cp Debug/*.dll "$stage_bin_debug"
            cp Debug/swiftshader/*.dll "$stage_bin_debug/swiftshader"
            cp Debug/*.bin "$stage_bin_debug"
            cp Debug/libcef.lib "$stage_lib_debug"
            cp Release/*.dll "$stage_bin_release"
            cp Release/*.bin "$stage_bin_release"
            cp Release/swiftshader/*.dll "$stage_bin_release/swiftshader"
            cp Release/libcef.lib "$stage_lib_release"
            cp -R Resources/* "$stage/resources"
            cp -R include/* "$stage/include/cef/include"
        popd
        pushd "dullahan"
            rm -rf build
            mkdir -p build
            pushd "build"
                cmake .. -G "Visual Studio 15" -DCMAKE_SYSTEM_VERSION="10.0.17134.0" \
                    -DCEF_INCLUDE_DIR="$(cygpath -w "$stage/include/cef/include")" \
                    -DCEF_LIB_DIR="$(cygpath -w "$stage/lib")" \
                    -DCEF_BIN_DIR="$(cygpath -w "$stage/bin")" \
                    -DCEF_RESOURCE_DIR="$(cygpath -w "$stage/resources")"
                cmake --build . --config "Release"

                cp "Release/dullahan.lib" "$stage_lib_release"
                cp "Release/dullahan_host.exe" "$stage_bin_release"
            popd
        popd
    ;;
    "windows64")
        pushd "cef"
            rm -rf build
            mkdir -p build
            pushd "build"
                cmake -G "Visual Studio 15 Win64" .. -DCMAKE_SYSTEM_VERSION="10.0.17134.0" -DCEF_RUNTIME_LIBRARY_FLAG="/MD"
                cmake --build . --target "libcef_dll_wrapper" --config "Debug"
                cmake --build . --target "libcef_dll_wrapper" --config "Release"

                cp libcef_dll_wrapper/Debug/libcef_dll_wrapper.* "$stage_lib_debug"
                cp libcef_dll_wrapper/Release/libcef_dll_wrapper.* "$stage_lib_release"
            popd
            cp Debug/*.dll "$stage_bin_debug"
            cp Debug/*.bin "$stage_bin_debug"
            cp Debug/swiftshader/*.dll "$stage_bin_debug/swiftshader"
            cp Debug/libcef.lib "$stage_lib_debug"
            cp Release/*.dll "$stage_bin_release"
            cp Release/*.bin "$stage_bin_release"
            cp Release/swiftshader/*.dll "$stage_bin_release/swiftshader"
            cp Release/libcef.lib "$stage_lib_release"
            cp -R Resources/* "$stage/resources"
            cp -R include/* "$stage/include/cef/include"
        popd
        pushd "dullahan"
            rm -rf build
            mkdir -p build
            pushd "build"
                cmake .. -G "Visual Studio 15 Win64" -DCMAKE_SYSTEM_VERSION="10.0.17134.0" \
                    -DCEF_INCLUDE_DIR="$(cygpath -w "$stage/include/cef/include")" \
                    -DCEF_LIB_DIR="$(cygpath -w "$stage/lib")" \
                    -DCEF_BIN_DIR="$(cygpath -w "$stage/bin")" \
                    -DCEF_RESOURCE_DIR="$(cygpath -w "$stage/resources")"
                cmake --build . --config "Release"

                cp "Release/dullahan.lib" "$stage_lib_release"
                cp "Release/dullahan_host.exe" "$stage_bin_release"
            popd
        popd
    ;;
esac

# Copy the headers
cp "dullahan/src/dullahan.h" "$stage/include/cef"
cp "dullahan/src/dullahan_version.h" "$stage/include/cef"

# Copy License (extracted from the readme)
cp "cef/LICENSE.txt" "$stage/LICENSES/CEF_LICENSE.txt"
cp "dullahan/LICENSE.txt" "$stage/LICENSES/dullahan.txt"
pass
