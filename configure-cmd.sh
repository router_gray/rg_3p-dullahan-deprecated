#!/bin/bash

DULLAHAN_VERSION="1.1.1085"
CEF_VERSION_FULL="3.3497.1833.g13f506f"

# turn on verbose debugging output for parabuild logs.
set -x
# make errors fatal
set -e

# Check autobuild is around or fail
if [ -z "$AUTOBUILD" ] ; then
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

# Load autobuild provided shell functions and variables
set +x
eval "$("$AUTOBUILD" source_environment)"
set -x

TOP="$(dirname "$0")"
pushd "${TOP}"

# Form the cef archive URL to fetch
case "$AUTOBUILD_PLATFORM" in
    "darwin")
    CEF_PLATFORM="macosx64"
    CEF_PACKAGE_EXTENSION="tar.bz2"
    CEF_MD5="c1188283334c0d13714264d8f5a1eec9"
    CEF_FOLDER_NAME="cef_binary_${CEF_VERSION_FULL}_${CEF_PLATFORM}"
    CEF_ARCHIVE="${CEF_FOLDER_NAME}.${CEF_PACKAGE_EXTENSION}"
    CEF_URL="http://opensource.spotify.com/cefbuilds/${CEF_ARCHIVE}"
    ;;
    "linux")
    CEF_PLATFORM="linux32"
    CEF_PACKAGE_EXTENSION="tar.bz2"
    CEF_MD5="5b525a7356aa276cc5a943cdc4309133"
    CEF_FOLDER_NAME="cef_binary_${CEF_VERSION_FULL}_${CEF_PLATFORM}"
    CEF_ARCHIVE="${CEF_FOLDER_NAME}.${CEF_PACKAGE_EXTENSION}"
    CEF_URL="http://opensource.spotify.com/cefbuilds/${CEF_ARCHIVE}"
    ;;
    "linux64")
    CEF_PLATFORM="linux64"
    CEF_PACKAGE_EXTENSION="tar.bz2"
    CEF_MD5="2e0c0f1af8880fdc857026fc521fd088"
    CEF_FOLDER_NAME="cef_binary_${CEF_VERSION_FULL}_${CEF_PLATFORM}"
    CEF_ARCHIVE="${CEF_FOLDER_NAME}.${CEF_PACKAGE_EXTENSION}"
    CEF_URL="http://opensource.spotify.com/cefbuilds/${CEF_ARCHIVE}"
    ;;
    "windows")
    CEF_PLATFORM="windows32"
    CEF_PACKAGE_EXTENSION="tar.bz2"
    CEF_MD5="e82132b51692f1613f741170d01ae409"
    CEF_FOLDER_NAME="cef_binary_${CEF_VERSION_FULL}_${CEF_PLATFORM}"
    CEF_ARCHIVE="${CEF_FOLDER_NAME}.${CEF_PACKAGE_EXTENSION}"
    CEF_URL="http://opensource.spotify.com/cefbuilds/${CEF_ARCHIVE}"
    ;;
    "windows64")
    CEF_PLATFORM="windows64"
    CEF_PACKAGE_EXTENSION="tar.bz2"
    CEF_MD5="58cfec1f2ae2f579fd50804f449df05e"
    CEF_FOLDER_NAME="cef_binary_${CEF_VERSION_FULL}_${CEF_PLATFORM}"
    CEF_ARCHIVE="${CEF_FOLDER_NAME}.${CEF_PACKAGE_EXTENSION}"
    CEF_URL="http://opensource.spotify.com/cefbuilds/${CEF_ARCHIVE}"
    ;;
esac

# Fetch and extract the cef archive
fetch_archive "${CEF_URL}" "${CEF_ARCHIVE}" "${CEF_MD5}"
extract ${CEF_ARCHIVE}
rm -rf cef
mv "$CEF_FOLDER_NAME" "cef"

stage="$(pwd)/stage"
echo "${DULLAHAN_VERSION}-${CEF_VERSION_FULL}" > "${stage}/VERSION.txt"

popd
